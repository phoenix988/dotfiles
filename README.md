# Dotfiles

###  __My personal dots for various programs I use.__

# TABLE OF CONTENT

- [ABOUT MY DOTFILES](#about-my-dotfiles)
- [CONFIGS](#configs)
    - [WINDOW MANAGERS](#window-managers)
    - [TEXT EDITORS](#text-editors)
        - [NEOVIM](#neovim)
        - [DOOM EMACS](#doom-emacs)
    - [OTHER CONFIGS](#other-configs)
        - [KITTY](#kitty)
- [SCRIPTS](#scripts)
    - [SYNC SCRIPT](#sync-script)
    - [TMUX SESSION](#tmux-session)
    - [TMUX JUMP](#tmux-jump)
    - [OTHERS](#others)
    - [MEDIA](#media)
        

## ABOUT MY DOTFILES

![Hyprland Desktop](./.images/hyprland-desktop.png)

This repository contains all my personal config files for software I use daily. Many of my configs are based on other people's configs, which I tweak to my liking. For example, my Qtile config is based on Distro Tube's Qtile config, with some keybindings adjustments and styling.

## CONFIGS

### WINDOW MANAGERS

All the Window Manager configs I have. However, 
the only two I have played around with and kept up to date is __`Hyprland`__,__`Qtile`__ and __`Awesome`__. The other configs might be a bit out of date since I haven't used them recently. So if you pull down my Xmonad, Awesome, or SpectrWM config, they might be broken.

- [Qtile](.config/qtile)
- [Xmonad](.xmonad)
- [Hyprland](.config/hypr)
- [SpectrWM](.config/spectrwm)
- [Awesome](.config/awesome)
- [LeftWM](.config/leftwm)

### TEXT EDITORS

The text editors I use is `Doom Emacs` and `Neovim`. `Doom Emacs` has a ton of features, while `Neovim` completely changed the way I use my computer with its fantastic vim motions.

- [NeoVim](.config/nvim)
- [Doom Emacs](.config/doom)

#

#### NEOVIM

![NeoVim](./.images/nvim.png)

#### DOOM EMACS

![Doom Emacs](./.images/doom.jpeg)

#
### OTHER CONFIGS
Here are some other config files I store here, such as different `shells`, `terminals`, `run launchers`, etc.

- [Waybar](.config/waybar)
- Zsh
- Bash
- [Fish](.config/fish)
- [Kitty](.config/kitty)
- [Alacritty](.config/alacritty)
- [Qutebrowser](.config/qutebrowser)
- [Wofi](.config/wofi)
- [Picom](.config/picom)
- [Rofi](.config/rofi)
- [Vifm](.config/vifm)
- Starship
- [Wlogout](.config/wlogout)
- Tmux
- [Dunst](.config/dunst)

#### KITTY

![Kitty](./.images/kitty.png)

## SCRIPTS

I also store some of my custom scripts here that I use to make my life a little bit easier.

### SYNC SCRIPT
This script quickly syncs some of my most popular scripts and configs to my repos.
- [Sync Scripts](.scripts/sync)
- [Sync Script Org](.scripts/sync/sync-script.org)

### TMUX SESSION
Script for `tmux` that will create a session if it doesn't exist already. If it does exist, this script will attach that session. Use it to quickly create some tmux windows.
- [Create Tmux Session](.scripts/activated/create-tmux-session.sh)

### TMUX JUMP

Some simple `tmux` script I wrote so I can create a keybinding in my window manager to quickly jump between tabs within a tmux session.

- [Tmux Jump](.scripts/tmux/window-(1-9))

### OTHERS

Also has some other scripts to set random wallpapers, control volume so I can bind it to my window manager easily as a keybinding.

### MEDIA

- [Medianext](.scripts/activated/medianext)
- [Mediaprev](.scripts/activated/mediaprev)
- [Mediaplay](.scripts/activated/mediaplay)
- [Mute Unmute](.scripts/activated/mute-unmute.sh)
- [Volume Up](.scripts/activated/volume-up.sh)
- [Volume Down](.scripts/activated/volume-down.sh)

### CREDITS

Credits to the people that wrote the config files that I used as a base.
