##____  _                      _
#|  _ \| |__   ___   ___ _ __ (_)_  __
#| |_) | '_ \ / _ \ / _ \ '_ \| \ \/ /
#|  __/| | | | (_) |  __/ | | | |>  <
#|_|   |_| |_|\___/ \___|_| |_|_/_/\_\
#
# -*- coding: utf-8 -*-
# Enable the subsequent settings only in interactive sessions
case $- in
  *i*) ;;
    *) return;;
esac


# Function to check if a command exist
command_exist() {
        type "$1" &> /dev/null;
}

export HISTFILE="${XDG_CONFIG_HOME}"/bash/history

# Preferred editor for local and remote sessions
 if [[ -n $SSH_CONNECTION ]]; then
   export EDITOR='nvim'
 else
   export EDITOR='nvim'
 fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-bash libs,
# plugins, and themes. Aliases can be placed here, though oh-my-bash
# users are encouraged to define aliases within the OSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#

# Run pfetch on launch
[ -f /usr/bin/fastfetch ] && fastfetch

alias sou="source ~/.bashrc"

# BEGIN_KITTY_SHELL_INTEGRATION
if test -n "$KITTY_INSTALLATION_DIR" -a -e "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"; then source "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"; fi
# END_KITTY_SHELL_INTEGRATION
[ -e "$HOME/.cargo/env" ] && . "$HOME/.cargo/env"


# Source my aliases
[ -e $HOME/.config/alias/alias.sh ] && source ~/.config/alias/alias.sh

# Makes it easier to extract files
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# Add some folders to PATH if they exist
if [ -d "$HOME/.bin" ] ;
  then PATH="$HOME/.bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ] ;
  then PATH="$HOME/.local/bin:$PATH"
fi

#add_myscripts="/home/karl/.scripts/activated"
#myscripts_exist=$(echo $PATH | sed 's/:/\n/g' | grep $add_myscripts)
#[ -z "$myscripts_exist" ] &&  PATH="$PATH:$add_myscripts"
#
#add_dmenu="/home/karl/.dmenu"
#dmenu_exist=$(echo $PATH | sed 's/:/\n/g' | grep $add_dmenu)
#[ -z "$dmenu_exist" ] &&  PATH="$PATH:$add_dmenu"
#
#add_doom="/home/karl/.config/emacs/bin/"
#doom_exist=$(echo $PATH | sed 's/:/\n/g' | grep $add_doom)
#[ -z "$doom_exist" ] &&  PATH="$PATH:$add_doom"

[ -e /usr/local/bin/starship ] && eval "$(starship init bash)"

# Starts zoxide if its installed
command_exist zoxide && eval "$(zoxide init --cmd cd bash)"
source "/home/karl/.local/share/cargo/env"
