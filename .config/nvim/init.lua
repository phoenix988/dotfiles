-----------------------------------------------
-- This file bootstraps everything           --
-- I keep this file as minimal as possible   --
-----------------------------------------------

----------------------------
-- Set spacebar as leader --
----------------------------
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

----------------------------
-- Import my config files --
----------------------------
require("karl")

---------------------------
-- Load plugins settings --
---------------------------
require("plugins")


