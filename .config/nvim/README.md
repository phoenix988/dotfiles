# Neovim

**Description:** Personal configuration for NVIM written in Lua  
**Author:** Karl Fredin

![Neovim](./images/Neovim-logo.svg.png)

## Table of Contents
- Neovim
- Install
- Plugins
- Keybindings
  - File Management
  - Theme
  - Source
  - Window
  - Git

## Neovim
Personal Neovim configuration written in Vim from scratch using the lazy plugin manager.

## Plugins
Plugins I use in my configuration

## Install
To install my config, run these commands. Remember to backup your current NVIM configs if you have them. Then launch Neovim 
and plugins will install automatically.

```sh
# Creating backup of existing config if you have
cp -r $HOME/.config/nvim $HOME/.config/nvim.bak

# Obs this will remove your Neovim config
rm -rf $HOME/.config/nvim

# Remove meta data to avoid problems
rm -rf $HOME/.local/share/nvim
rm -rf $HOME/.cache/nvim

git clone -b neovim https://github.com/phoenix988/dotfiles.git ~/.config/nvim
```

## KEYBINDINGS
Some of the most custom important keybindings to learn. 

### FILE MANAGEMENT
 Commands to quickly move around files in neovim
 | Command                | Description                     | Keybinding  |
 |------------------------|---------------------------------|-------------|
 | Yazi                   | Open yazi file manager          | SPC .       |
 | Telescope find_files    | Find files in telescope         | SPC f f     |
 | Telescope recent_files  | Find recent files               | SPC f r     |
 | mark.add_file           | Add a file in harpoon           | SPC a       |
 | ui.toggle_quick_menu    | Open harpoon menu               | C-e         |
 | ui.nav_file(1-9)        | Navigate the harpoon files      | SPC-h (1-9) |
 | Grep                    | Grep a string of text in a file | SPC f g     |
 | NerdTreeToggleFocus     | Focus NerdTree                  | F6          |
 | NeoTreeToggle           | Toggle NeoTree                  | SPC f t     |
 | NeoTreeFloat            | Toggle NeoTree in floating mode | SPC n f     |
 | Neotree .               | Open Neotree in current dir     | SPC n .     |
 | Neotree ~               | Open Neotree in home            | SPC n h     |

### THEME
 Commands to change theme plus highlight colors
 | Command               | Description            | Keybinding |
 |-----------------------|------------------------|------------|
 | HighlightColorsToggle | Toggle color highlight | SPC c h    |
 | Telescope colorscheme | Change colorscheme     | SPC h t    |

### SOURCE
 Update plugins and source new config using these commands
 | Command    | Description     | Keybinding |
 |------------|-----------------|------------|
 | PackerSync | Sync Plugins    | SPC h r    |
 | Source     | Source lua file | SPC h s    |

### WINDOW
 Create new tabs and manipulate buffers using these commands
 | Command     | Description       | Keybinding |
 |-------------|-------------------|------------|
 | BufferNext  | Next buffer       | SPC b n    |
 | BufferPrev  | Previous Buffer   | SPC b p    |
 | BufferClose | Kill buffer       | SPC b k    |
 | Telescope buffers |buffer list       | SPC b l    |
 | Tabnew      | Open new tab      | SPC t n    |
 | Tabnext      | Next tab      | SPC t g    |
 | Tabprevious      | Previous tab      | SPC t G    |
 | Tabclose    | Close current tab | SPC t d    |
 | Dashboard   | Move to dashboard | SPC d      |

### GIT
  Git management in neovim
 | Command             | Description              | Keybinding |
 |---------------------|--------------------------|------------|
 | Telescope git_files | Find files in a git repo | SPC g f    |
 | Git                 | Open git status          | SPC g g    |


