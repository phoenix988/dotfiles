local Attach = {}

Attach.with = {
  default = require("lsp.attach.with_default"),
  extension = {
    signature = require("lsp.attach.with_lsp_signature"),
    navic = require("lsp.attach.with_navic"),
  },
  all = require("lsp.attach.with_all_extension"),
}

return Attach
