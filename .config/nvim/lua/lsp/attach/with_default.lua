return function(client, bufnr)
  require("lsp.command.buffer").attach(client, bufnr)
  require("lsp.command.diagnostic").attach(bufnr)

  if client.server_capabilities.codeLensProvider then
    vim.lsp.codelens.refresh()
    require("lsp.command.codelens").attach(bufnr)
  end

  require("lsp.attach.with_keymap").attach(client, bufnr)
  require("lsp.event").attach(client, bufnr)
  require("lsp.diagnostic").attach(bufnr)

  vim.api.nvim_buf_set_var(bufnr, "lsp_attached", true)
end
