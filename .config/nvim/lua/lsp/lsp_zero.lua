-- configuring lsp zero and mason lsp servers
local lsp_zero = require('lsp-zero')

local lsp_attach = function(client, bufnr)
  lsp_zero.default_keymaps({buffer = bufnr})
end

lsp_zero.extend_lspconfig({
  lsp_attach = lsp_attach,
})

-- When you don't have mason.nvim installed
-- You'll need to list the servers installed in your system
lsp_zero.setup_servers { 'eslint', 'lua_ls', 'ansiblels', 'bashls', 'yamlls' }

lsp_zero.extend_lspconfig({
  sign_text = true,
  lsp_attach = lsp_attach,
})

require('lspconfig').biome.setup({
  single_file_support = false,
  on_attach = function(client, bufnr)
    print('hello biome')
  end
})

lsp_zero.setup()

local cmp = require 'cmp'
local cmp_select = { behavior = cmp.SelectBehavior.Select }

-- Nvim-cmp setup
cmp.setup {
  snippet = {
    expand = function(args)
      require('luasnip').lsp_expand(args.body)
    end,
  },
  window = {
    completion = cmp.config.window.bordered(),
    documentation = cmp.config.window.bordered(),
  },
  sources = cmp.config.sources({
    { name = 'nvim_lsp' },
    { name = 'luasnip' }, -- For luasnip users.
  }, {
    { name = 'buffer' },
  }),
}

