------------------------------
-- Load lazy plugin manager --
------------------------------
---@diagnostic disable-next-line: different-requires
require("karl.lazy")

---------------------------------------------
-- Remap and important settings for neovim --
---------------------------------------------
require("karl.remap")
require("karl.options")

------------------------------
-- Custom functions I wrote --
------------------------------
require("karl.dash")
require("karl.session")
