-- Install package manager

--    https://github.com/folke/lazy.nvim
--    `:help lazy.nvim.txt` for more info
local lazypath = vim.fn.stdpath 'data' .. '/lazy/lazy.nvim'

local a = vim.api

-- url to my gitlab
local gitlab = 'https://gitlab.com/phoenix988/'

---@diagnostic disable-next-line: undefined-field
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system {
    'git',
    'clone',
    '--filter=blob:none',
    'https://github.com/folke/lazy.nvim.git',
    '--branch=stable', -- latest stable release
    lazypath,
  }
end

vim.opt.rtp:prepend(lazypath)

-- Install plugins here
-- Some plugin settings are stored here but for more complex settings
-- I make a seperate file in after/plugin directoory
require('lazy').setup({
  -- NVChad UI
  'nvim-lua/plenary.nvim',

  {
    'nvchad/ui',
    config = function()
      require 'nvchad'
    end,
  },

  {
    'nvchad/base46',
    lazy = true,
    build = function()
      require('base46').load_all_highlights()
    end,
  },

  { -- Markdown
    'saimo/peek.nvim',
    event = { 'VeryLazy' },
    build = 'deno task --quiet build:fast',
    config = function()
      require('peek').setup()
      a.nvim_create_user_command('PeekOpen', require('peek').open, {})
      a.nvim_create_user_command('PeekClose', require('peek').close, {})
      app = 'microsoft-edge-stable'
    end,
  },
  -- My plugin
  {
    gitlab .. 'grepsync',
    dir = '~/myrepos/GrepSync',
    config = function()
      require('grepsync').setup {
        line_numbers = true,
        keymaps = {
          ['q'] = 'actions.quit',
          ['<CR>'] = 'actions.enter_file',
        },
      }
    end,
  },

  -- Git related plugins
  'tpope/vim-fugitive',
  'tpope/vim-rhubarb',
  {
    'NeogitOrg/neogit',
    config = true,
  },

  --{
  --  'nvim-neorg/neorg',
  --  dependencies = { 'nvim-lua/plenary.nvim', 'vhyrro/luarocks.nvim' },
  --},

  {
    'kdheepak/lazygit.nvim',
    -- optional for floating window border decoration
    dependencies = {
      'nvim-lua/plenary.nvim',
    },
  },

  { -- Adds git releated signs to the gutter, as well as utilities for managing changes
    'lewis6991/gitsigns.nvim',
    opts = {
      -- See `:help gitsigns.txt`
      signs = {
        add = { text = '+' },
        change = { text = '~' },
        delete = { text = '_' },
        topdelete = { text = '‾' },
        changedelete = { text = '~' },
      },
    },
  },

  -- Buffer list
  'roblillack/vim-bufferlist',

  -- Detect tabstop and shiftwidth automatically
  'tpope/vim-sleuth',

  -- Wilder vim
  {
    'gelguy/wilder.nvim',
    config = function()
      -- config goes here
    end,
  },
  -- Dashboard settings
  {
    'goolord/alpha-nvim', -- Using alpha as my dashboard
    dependencies = {
      'nvim-tree/nvim-web-devicons',
    },
    event = 'VimEnter',
    opts = function() end,
    config = function() end,
  }, -- End of dashboard config

  -- NOTE: This is where your plugins related to LSP can be installed.
  --  The configuration is done below. Search for lspconfig to find it below.
  { 'VonHeikemen/lsp-zero.nvim', dependencies = { 'neovim/nvim-lspconfig' } },
  { 'williamboman/mason-lspconfig.nvim' },
  { -- LSP Configuration & Plugins
    'neovim/nvim-lspconfig',
    dependencies = {
      -- Automatically install LSPs to stdpath for neovim
      { 'williamboman/mason.nvim', config = true },
      'williamboman/mason-lspconfig.nvim',

      -- Useful status updates for LSP
      -- NOTE: `opts = {}` is the same as calling `require('fidget').setup({})`
      { 'j-hui/fidget.nvim', opts = {} },

      -- Additional lua configuration, makes nvim stuff amazing!
      'folke/neodev.nvim',
    },
  },

  { -- Autocompletion
    'hrsh7th/nvim-cmp',
    event = 'InsertEnter',
    lazy = true,
    dependencies = { 'hrsh7th/cmp-nvim-lsp', 'L3MON4D3/LuaSnip', 'saadparwaiz1/cmp_luasnip' },
  },
  { 'hrsh7th/cmp-nvim-lsp' }, -- Required
  { 'hrsh7th/cmp-buffer' }, -- Optional
  { 'hrsh7th/cmp-path' }, -- Optional
  { 'saadparwaiz1/cmp_luasnip' }, -- Optional
  { 'hrsh7th/cmp-nvim-lua' }, -- Optional

  -- Snippets
  { 'L3MON4D3/LuaSnip' }, -- Required
  { 'rafamadriz/friendly-snippets' }, -- Optional
  -- End of Lsp configuration

  -- Highlight colors inside of vim
  { 'brenoprata10/nvim-highlight-colors' },

  -- Orgmode
  { 'nvim-orgmode/orgmode' },

  -- Save as sudo
  { 'lambdalisue/suda.vim' },

  -- Terminal toggle
  { 'akinsho/toggleterm.nvim' },

  -- Useful plugin to show you pending keybinds.
  {
    'folke/which-key.nvim',
    opts = {},
  },

  { -- Some more themes
    'navarasu/onedark.nvim',
    'https://gitlab.com/phoenix988/iceberg.nvim',
    'rose-pine/neovim',
    'Mofiqul/dracula.nvim',
    'folke/tokyonight.nvim',
    'shaunsingh/nord.nvim',
    'ribru17/bamboo.nvim',
    'rmehri01/onenord.nvim',
    'catppuccin/nvim',
    'romgrk/doom-one.vim',
  },

  { 'zaldih/themery.nvim' },

  -- "gc" to comment visual regions/lines
  { 'numToStr/Comment.nvim', opts = {} },

  -- Quickly navigate files
  -- Fuzzy Finder (files, lsp, etc)
  { 'nvim-telescope/telescope.nvim', version = '*', dependencies = { 'nvim-lua/plenary.nvim' } },
  { 'smartpde/telescope-recent-files' },
  { 'nvim-telescope/telescope-file-browser.nvim', version = '*', dependencies = { 'nvim-lua/plenary.nvim' } },

  -- Fuzzy Finder Algorithm which requires local dependencies to be built.
  -- Only load if `make` is available. Make sure you have the system
  -- requirements installed.
  {
    'nvim-telescope/telescope-fzf-native.nvim',
    -- NOTE: If you are having trouble with this installation,
    -- refer to the README for telescope-fzf-native for more instructions.
    build = 'make',
    cond = function()
      return vim.fn.executable 'make' == 1
    end,
  },

  -- Harpoon
  { 'ThePrimeagen/harpoon' },

  -- Vifm/yazi file manager
  { 'vifm/vifm.vim' },
  { 'DreamMaoMao/yazi.nvim' },

  -- Undotree to see history of a file
  { 'mbbill/undotree' },

  -- File Tree
  { 'nvim-tree/nvim-web-devicons' },
  { -- Neotree
    'nvim-neo-tree/neo-tree.nvim',
    dependencies = { 'nvim-lua/plenary.nvim', 'MunifTanjim/nui.nvim' },
  },

  -- Which key gives hint about keybindings
  { 'folke/which-key.nvim' },

  { -- Lastplace remeber your last posisition
    'ethanholz/nvim-lastplace',
    config = function()
      require('nvim-lastplace').setup {
        lastplace_ignore_buftype = { 'quickfix', 'nofile', 'help' },
        lastplace_ignore_filetype = { 'gitcommit', 'gitrebase', 'svn', 'hgcommit' },
        lastplace_open_folds = true,
      }
    end,
  },

  { -- Highlight, edit, and navigate code
    'nvim-treesitter/nvim-treesitter',
    dependencies = {
      'nvim-treesitter/nvim-treesitter-textobjects',
    },
    build = ':TSUpdate',
  },

  {
    'kylechui/nvim-surround',
    config = function()
      require('nvim-surround').setup {
        -- Configuration here, or leave empty to use defaults
      }
    end,
  },

  -- Vimwiki
  { 'chipsenkbeil/vimwiki.nvim' },

  -- nvim-compe lsp plugin
  { 'hrsh7th/nvim-compe' },

  -- None ls
  { 'nvimtools/none-ls.nvim' },

  {
    'Exafunction/codeium.nvim',
    dependencies = {
      'nvim-lua/plenary.nvim',
      'hrsh7th/nvim-cmp',
    },
    config = function()
      require('codeium').setup {}
    end,
  },

  { -- Auto Sessions
    'rmagatti/auto-session',
    config = function()
      require('auto-session').setup {
        auto_sessions_supress_dirs = { '~/', '~/Downloads', '~/Documents', '~/.cache' },
        session_lents = {
          buftypes_to_ignore = {},
          load_on_setup = true,
          theme_conf = { border = true },
          previewer = false,
        },
      }
    end,
  },

  -- Oil
  { 'stevearc/oil.nvim' },

  -- Tmux navigator
  { 'christoomey/vim-tmux-navigator' },

  { -- Silicon snapshot
    'michaelrommel/nvim-silicon',
    lazy = true,
    cmd = 'Silicon',
    config = function()
      require('silicon').setup {
        font = 'JetBrainsMono Nerd Font=34',
        theme = 'Nord',
        background = '#444B71',
        window_title = function()
          return vim.fn.fnamemodify(a.nvim_buf_get_name(a.nvim_get_current_buf()), ':t')
        end,
      }
    end,
  },

  { -- DAP Debuggers
    'mfussenegger/nvim-dap',
    dependencies = {
      'rcarriga/nvim-dap-ui',
      'nvim-neotest/nvim-nio',
      'mfussenegger/nvim-dap-python',
    },
  },

  {
    "gerazov/ollama-chat.nvim",
    dependencies = {
      "nvim-lua/plenary.nvim",
      "stevearc/dressing.nvim",
      "nvim-telescope/telescope.nvim",
    }
  },
  {
   'jpmcb/nvim-llama'
  },
  {
  'epwalsh/obsidian.nvim',
    opts = {
      workspaces = {
        {
          name = "personal",
          path = "~/Documents/Notes/Personal",
        },
        {
          name = "work",
          path = "~/Documents/Notes/Work",
        },
      },
  }

  },

}, {})
