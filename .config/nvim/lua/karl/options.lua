local o = vim.opt

vim.wo.relativenumber = true
o.nu = true

o.tabstop = 6
o.softtabstop = 4
o.shiftwidth = 4
o.expandtab = true

o.smartindent = true

o.wrap = false

o.swapfile = false
o.backup = false
o.undodir = os.getenv("HOME") .. "/.config/vim/undodir"
o.undofile = true

o.hlsearch = false
o.incsearch = true

o.termguicolors = true

o.scrolloff = 8
o.signcolumn = "yes"
o.isfname:append("@-@")

o.updatetime = 50
--
o.updatetime = 50

vim.o.modifiable = true

o.clipboard = "unnamedplus"
o.paste = true
o.mouse = 'a'

o.guifont = "JetBrainsMono Nerd Font:h15" -- Replace with your preferred font and size

-- set transparency
if vim.g.neovide then
  vim.g.neovide_transparency = 0.95  -- Adjust the transparency level as needed
else
  --vim.api.nvim_set_hl(0, "Normal", { bg = "none" })
  --vim.api.nvim_set_hl(0, "NormalFloat", { bg = "none" })
end


