local M = {}

local a = vim.api

-- custom functions to save sessions
function M.saveSession()
  local sessionFile = "last_session"
  local sessionPath = vim.fn.expand('~/.local/share/nvim/' .. sessionFile)
  vim.cmd('mksession! ' .. sessionPath)
  print('Session saved to: ' .. sessionPath)
end

-- custom functions to load sessions
function M.loadSession()
  local sessionFile = "last_session"
  local sessionPath = vim.fn.expand('~/.local/share/nvim/' .. sessionFile)
  vim.cmd('source ' .. sessionPath)
  print('Session loaded from: ' .. sessionPath)
end

-- Bootstrap the functions
function M.init()
  a.nvim_create_user_command('SaveSession', function()
    M.saveSession()
  end, {
  nargs = 0,
  })

  a.nvim_create_user_command('LoadSession', function()
    M.loadSession()
  end, {
  nargs = 0,
  })
end

M.init()

return M
