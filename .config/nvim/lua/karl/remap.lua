-- Set your keybinds in this file

-- File for keybinds that is not dependant on any plugins
-- For keybinds related to any plugins you set them in after/plugins/keymap
local map = vim.keymap.set

-- Source a file
map('n', '<leader>hs', vim.cmd.so, { desc = "Reload config"})

-- Basic Editing shortcuts that are useful
map('v', 'J', ":m '>+1<CR>gv=gv")
map('v', 'K', ":m '<-2<CR>gv=gv")

map('n', '<C-d>', '<C-d>zz')
map('n', '<C-u>', '<C-u>zz')

map('x', '<leader>p', '"_dp', { desc = 'Paste/Delete without Yanking' })
map('n', '<leader>dw', '"_dw', { desc = 'Delete word without Yanking' })

map('n', '<leader>d', '"_d', { desc = 'Delete without yanking' })
map('v', '<leader>d', '"_d', { desc = 'Delete without yanking' })

map('n', '<C-f>', '<cmd>silent !tmux neww tmux-sessionizer<CR>')

map('n', '<leader>sc', [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]],
  { desc = 'Change all occurrences of selected word' })
map('n', '<leader>wc', [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]],
  { desc = 'Change all occurrences of selected word' })
map('n', '<leader>wd', '"_d', { desc = 'Delete without yanking' })
map('n', '<leader>x', '<cmd>!chmod +x %<CR>', { silent = true, desc = 'Make file executable' })
map('n', '<leader>fx', '<cmd>!chmod +x %<CR>', { silent = true, desc = 'Make file executable' })

-- Save/load Session custom
map('n', '<Leader>ss', ':SaveSession<CR>', { noremap = true, silent = true })
map('n', '<Leader>sl', ':LoadSession<CR>', { noremap = true, silent = true })

-- Yazi
map('n', '<leader>.', ':Oil<CR>', { noremap = true, silent = true })
map('n', '<leader>fm', ':Yazi<CR>', { noremap = true, silent = true })

-- Quickly move between buffers
map('n', '<leader>bn', ':bn<CR>', { noremap = true, silent = true, desc = 'Next Buffer' })
map('n', '<leader>bm', ':bp<CR>', { noremap = true, silent = true, desc = 'Previous Buffer' })
map('n', '<leader>bP', ':bp<CR>', { noremap = true, silent = true, desc = 'Previous Buffer' })
map('n', '<leader>bk', ':bdelete<CR>', { noremap = true, silent = true, desc = 'Kill buffer' })
map('n', '<leader>bl', ':Telescope buffers<CR>', { noremap = true, silent = true })

-- Manage tabs
map('n', '<Leader>tn', ':tabnew<CR>', { noremap = true, silent = true, desc = 'New Tab' })
map('n', '<Leader>td', ':tabclose<CR>', { noremap = true, silent = true, desc = 'Close Tab' })
--map('n', '<Leader>ch', ':HighlightColors on<CR>',{ noremap = true, silent = true, desc = 'Toggle Hex Colors on/off' })
map('n', 'tn', ':tabnext<CR>', { noremap = true, silent = true, desc = 'Next Tab' })
map('n', 'tp', ':tabprevious<CR>', { noremap = true, silent = true, desc = 'Previous Tab' })

-- Tab between splits
map('n', '<Tab>', '<C-W>w', { noremap = true, silent = true })
map('n', '<S-Tab>', '<C-W>W', { noremap = true, silent = true })

-- Git commands
map('n', '<leader>gs', ':Git<CR>', { noremap = true, silent = true })
map('n', '<leader>gg', ':Neogit<CR>', { noremap = true, silent = true })
map('n', '<leader>gb', ':Git blame<CR>', { noremap = true, silent = true })
map('n', '<leader>gs', ':Gitsigns preview_hunk<CR>', { noremap = true, silent = true })
map('n', '<Leader>gp', ':Git push<CR>', { noremap = true, silent = true })
map('n', '<Leader>gP', ':Git pull<CR>', { noremap = true, silent = true })
map('n', '<Leader>gd', ':Gdiff %<CR>', { noremap = true, silent = true })
map('n', '<leader>gl', ':LazyGit<CR>', { noremap = true, silent = true })

-- Neotree keybindings
map('n', '<F5>', ':Neotree toggle<CR>', { noremap = true, silent = true })
map('n', '<Leader>nt', ':Neotree toggle<CR>', { noremap = true, silent = true })
map('n', '<Leader>nf', ':Neotree float<CR>', { noremap = true, silent = true })
map('n', '<Leader>ns', ':Neotree ~/myrepos/setup<CR>', { noremap = true, silent = true })
map('n', '<Leader>na', ':Neotree ~/myrepos/azla<CR>', { noremap = true, silent = true })
map('n', '<Leader>nd', ':Neotree ~/myrepos/dotfiles<CR>', { noremap = true, silent = true })
map('n', '<Leader>ng', ':Neotree ~/myrepos<CR>', { noremap = true, silent = true })
map('n', '<Leader>nc', ':Neotree ~/.config<CR>', { noremap = true, silent = true })
map('n', '<Leader>nn', ':Neotree ~/.config/nvim<CR>', { noremap = true, silent = true })
map('n', '<Leader>nh', ':Neotree ~<CR>', { noremap = true, silent = true })
map('n', '<Leader>n.', ':Neotree .<CR>', { noremap = true, silent = true })

-- Oil
map('n', '<Leader>fo', ':Oil<CR>', { noremap = true, silent = true, desc = 'Oil file tree' })

-- Update plugins using lazy
map('n', '<Leader>hr', ':Lazy<CR>', { noremap = true, silent = true, desc = 'Lazy Plugin Menu' })

-- Terminal
map('n', '<Leader>tt', ':terminal<CR>', { noremap = true, silent = true, desc = 'Open Terminal' })
map('n', '<Leader>ok', ':terminal<CR>', { noremap = true, silent = true, desc = 'Open Terminal' })

-- Keybindings for Harpoon
local mark = require 'harpoon.mark'
local ui = require 'harpoon.ui'
map('n', '<Leader>h1', function() ui.nav_file(1) end, { noremap = true, silent = true, desc = "Jump to harpoon 1" })
map('n', '<Leader>h2', function() ui.nav_file(2) end, { noremap = true, silent = true, desc = "Jump to harpoon 2" })
map('n', '<Leader>h3', function() ui.nav_file(3) end, { noremap = true, silent = true, desc = "Jump to harpoon 3" })
map('n', '<Leader>h4', function() ui.nav_file(4) end, { noremap = true, silent = true, desc = "Jump to harpoon 4" })
map('n', '<Leader>h5', function() ui.nav_file(5) end, { noremap = true, silent = true, desc = "Jump to harpoon 5" })
map('n', '<Leader>h6', function() ui.nav_file(6) end, { noremap = true, silent = true, desc = "Jump to harpoon 6" })
map('n', '<Leader>h7', function() ui.nav_file(7) end, { noremap = true, silent = true, desc = "Jump to harpoon 7" })
map('n', '<Leader>h8', function() ui.nav_file(8) end, { noremap = true, silent = true, desc = "Jump to harpoon 8" })
map('n', '<Leader>h9', function() ui.nav_file(9) end, { noremap = true, silent = true, desc = "Jump to harpoon 9" })
map('n', '<Leader>p1', function() ui.nav_file(1) end, { noremap = true, silent = true, desc = "Jump to harpoon 1" })
map('n', '<Leader>p2', function() ui.nav_file(2) end, { noremap = true, silent = true, desc = "Jump to harpoon 2" })
map('n', '<Leader>p3', function() ui.nav_file(3) end, { noremap = true, silent = true, desc = "Jump to harpoon 3" })
map('n', '<Leader>p4', function() ui.nav_file(4) end, { noremap = true, silent = true, desc = "Jump to harpoon 4" })
map('n', '<Leader>p5', function() ui.nav_file(5) end, { noremap = true, silent = true, desc = "Jump to harpoon 5" })
map('n', '<Leader>p6', function() ui.nav_file(6) end, { noremap = true, silent = true, desc = "Jump to harpoon 6" })
map('n', '<Leader>p7', function() ui.nav_file(7) end, { noremap = true, silent = true, desc = "Jump to harpoon 7" })
map('n', '<Leader>p8', function() ui.nav_file(8) end, { noremap = true, silent = true, desc = "Jump to harpoon 8" })
map('n', '<Leader>p9', function() ui.nav_file(9) end, { noremap = true, silent = true, desc = "Jump to harpoon 9" })
map('n', '<leader>a', mark.add_file, { desc = 'Add file to harpoon' })
map('n', '<C-e>', ui.toggle_quick_menu, { desc = 'Harpoon Quick File Menu' })
map('n', '<leader>pa', mark.add_file, { desc = 'Harpoon Add file' })
map('n', '<leader>pl', ui.toggle_quick_menu, { desc = 'Harpoon Quick File Menu' })
------------------------------------------------------

-- Keybinding to open UndoTree
map('n', '<Leader>u', ':UndotreeToggle<CR>', { noremap = true, silent = true })

-- Keybinding to save as sudo
map('n', '<Leader>fs', ':SudaWrite<CR>', { noremap = true, silent = true, desc = 'Save as sudo' })

-- Keybindings for telescope
local builtin = require 'telescope.builtin'
require('telescope').load_extension 'recent_files'
require('telescope').load_extension 'file_browser'
map('n', '<Leader>ff', ':Telescope find_files<CR>',
  { noremap = true, silent = true, desc = 'Find Files' })
map('n', '<Leader>gf', ':Telescope git_files<CR>',
  { noremap = true, silent = true, desc = 'Git Files' })
map('n', '<Leader>ft', ':Telescope git_files<CR>',
  { noremap = true, silent = true, desc = 'Git Files' })
map('n', '<Leader>ht', ':Telescope themes<CR>',
  { noremap = true, silent = true, desc = 'Colorschemes' })
map('n', '<Leader>th', ':Telescope themes<CR>',
  { noremap = true, silent = true, desc = 'Colorschemes' })
map('n', '<Leader>fb', ':Telescope file_browser<CR>',
  { noremap = true, silent = true, desc = 'File Browser' })
map('n', '<Leader>fG', ':Telescope live_grep<CR>',
  { noremap = true, silent = true, desc = 'Live Grep for string' })
map('n', '<Leader>ma', ':Telescope mark<CR>',
  { noremap = true, silent = true, desc = 'Bookmarks' })
map('n', '<Leader>fw', ':Telescope live_grep<CR>',
  { noremap = true, silent = true, desc = 'Live grep' })
map('n', '<Leader>fo', ':Telescope oldfiles<CR>',
  { noremap = true, silent = true, desc = 'Recent files' })

map(
  'n',
  '<Leader>fr',
  [[<cmd>lua require('telescope').extensions.recent_files.pick()<CR>]],
  { noremap = true, silent = true, desc = 'Recent Files' }
)
-------------------------------------------------------------  

-- Custom grep command
map('n', '<Leader>fg', function()
  builtin.grep_string { search = vim.fn.input 'Grep >' }
end)

-- ORGMODE Tangle
map('n', '<Leader>oT', ':OrgTangle<CR>',
  { noremap = true, silent = true, desc = 'Tangle Org Document' })

-- Terminal mode
map('t', '<Esc>l', '<C-\\><C-n>', { noremap = true })

-- Session
-- List all your sessions / Projects
map('n', '<Leader>sm', require('auto-session.session-lens').search_session, {
  noremap = true,
})

map('n', '<Leader>pp', require('auto-session.session-lens').search_session, {
  noremap = true,
})

-- None-ls format
map('n', '<leader>fF', vim.lsp.buf.format, {})
map('n', '<Leader>hm', ':Mason<CR>', { noremap = true, silent = true, desc = 'Mason Meny' })

-- Co-pilot
map('i', '<C-/>', 'copilot#Accept(“<CR>”)', { expr = true, silent = true })

-- Tmux navigation
map('n', '<C-h', ':TmuxNavigateLeft<CR>', { noremap = true, silent = true })
map('n', '<C-l', ':TmuxNavigateRight<CR>', { noremap = true, silent = true })
map('n', '<C-j', ':TmuxNavigateDown<CR>', { noremap = true, silent = true })

-- Silicon
map('v', '<Leader>S', ':Silicon<CR>', { noremap = true, silent = true })

-- Peek
map('n', '<Leader>mo', ':PeekOpen<CR>', { noremap = true, silent = true, desc = 'Open Markdown Preview' })
map('n', '<Leader>mc', ':PeekClose<CR>', { noremap = true, silent = true, desc = 'Close Markdown Preview' })

-- My custom plugin to edit multiple files at once
map("n", "<leader>fa", ":GrepSync<CR>", {desc = "GrepSync"})

-- NVchad ui plugin
map("n", "<leader>hk", ":NvCheatsheet<CR>", {desc = "Cheatseet for keybinds"})
map("n", "<leader>no", ":Nvdash<CR>", {desc = "Nvchad dashboard"})
map("n", "<leader>l", ":Alpha<CR>", {desc = "Alpha Dashboard"})

-- Code actions
map('n', '<leader>ca', vim.lsp.buf.code_action, { desc = 'Perform Code Action' })

-- Define keymaps for nvim-dap
local dap = require('dap')
map('n', '<Leader>bp', dap.toggle_breakpoint, {desc = 'DAP toggle breakpoint'})
map('n', '<Leader>bc', dap.continue, {desc = "DAP continue"})
map('n', '<Leader>bo', dap.step_over, {desc = "DAP step over"})
map('n', '<Leader>bO', dap.step_out, {desc = "DAP step out"})

