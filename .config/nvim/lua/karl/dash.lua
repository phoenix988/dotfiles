local os = require 'os'

-- Local function to set the logo of my alpa dashboard for neovim
-- Requires you to configure alpha dashboard plugin before you can use it
-- You can probably also use any other plugin that lets you set a custom logo
local M = {}

function M.setLogo()
  local dayOfWeek = os.date '%A'
  if dayOfWeek == 'Friday' or dayOfWeek == 'friday' then
    local logo = [[
            ███████╗██████╗ ██╗██████╗  █████╗ ██╗   ██╗
            ██╔════╝██╔══██╗██║██╔══██╗██╔══██╗╚██╗ ██╔╝
            █████╗  ██████╔╝██║██║  ██║███████║ ╚████╔╝
            ██╔══╝  ██╔══██╗██║██║  ██║██╔══██║  ╚██╔╝
            ██║     ██║  ██║██║██████╔╝██║  ██║   ██║
            ╚═╝     ╚═╝  ╚═╝╚═╝╚═════╝ ╚═╝  ╚═╝   ╚═╝
            ]]
    return logo
  elseif dayOfWeek == 'Thursday' or dayOfWeek == 'thursday' then
    local logo = [[
            ████████╗██╗  ██╗██╗   ██╗██████╗ ███████╗██████╗  █████╗ ██╗   ██╗
            ╚══██╔══╝██║  ██║██║   ██║██╔══██╗██╔════╝██╔══██╗██╔══██╗╚██╗ ██╔╝
               ██║   ███████║██║   ██║██████╔╝███████╗██║  ██║███████║ ╚████╔╝
               ██║   ██╔══██║██║   ██║██╔══██╗╚════██║██║  ██║██╔══██║  ╚██╔╝
               ██║   ██║  ██║╚██████╔╝██║  ██║███████║██████╔╝██║  ██║   ██║
               ╚═╝   ╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═╝╚══════╝╚═════╝ ╚═╝  ╚═╝   ╚═╝
             ]]

    return logo
  elseif dayOfWeek == 'Saturday' or dayOfWeek == 'saturday' then
    local logo = [[
          _________   ____________________ _____________________      _____ _____.___.
         /   _____/  /  _  \__    ___/    |   \______   \______ \    /  _  \\__  |   |
         \_____  \  /  /_\  \|    |  |    |   /|       _/|    |  \  /  /_\  \/   |   |
         /        \/    |    \    |  |    |  / |    |   \|    `   \/    |    \____   |
        /_______  /\____|__  /____|  |______/  |____|_  /_______  /\____|__  / ______|
                \/         \/                         \/        \/         \/\/       
             ]]
    return logo
  elseif dayOfWeek == 'Monday' or dayOfWeek == 'monday' then
    local logo = [[
                                                    ___  
                                                 ,o88888 
                                              ,o8888888' 
                        ,:o:o:oooo.        ,8O88Pd8888"  
                    ,.::.::o:ooooOoOoO. ,oO8O8Pd888'"    
                  ,.:.::o:ooOoOoOO8O8OOo.8OOPd8O8O"      
                 , ..:.::o:ooOoOOOO8OOOOo.FdO8O8"        
                , ..:.::o:ooOoOO8O888O8O,COCOO"          
               , . ..:.::o:ooOoOOOO8OOOOCOCO"            
                . ..:.::o:ooOoOoOO8O8OCCCC"o             
                   . ..:.::o:ooooOoCoCCC"o:o             
                   . ..:.::o:o:,cooooCo"oo:o:            
                `   . . ..:.:cocoooo"'o:o:::'            
                .`   . ..::ccccoc"'o:o:o:::'             
               :.:.    ,c:cccc"':.:.:.:.:.'              
             ..:.:"'`::::c:"'..:.:.:.:.:.'               
           ...:.'.:.::::"'    . . . . .'                 
          .. . ....:."' `   .  . . ''                    
        . . . ...."'                                     
        .. . ."'                                         
       .                                                 
                                                         
             ]]
    return logo
  else
    local logo = [[
            ██████╗  █████╗ ███████╗██╗  ██╗██████╗  ██████╗  █████╗ ██████╗ ██████╗
            ██╔══██╗██╔══██╗██╔════╝██║  ██║██╔══██╗██╔═══██╗██╔══██╗██╔══██╗██╔══██╗
            ██║  ██║███████║███████╗███████║██████╔╝██║   ██║███████║██████╔╝██║  ██║
            ██║  ██║██╔══██║╚════██║██╔══██║██╔══██╗██║   ██║██╔══██║██╔══██╗██║  ██║
            ██████╔╝██║  ██║███████║██║  ██║██████╔╝╚██████╔╝██║  ██║██║  ██║██████╔╝
            ╚═════╝ ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝╚═════╝  ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═════╝
            ]]

    return logo
  end
end

return M
