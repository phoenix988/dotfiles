local setlogo = require('karl.dash').setLogo
local dashboard = require 'alpha.themes.dashboard'
local myConfig = '/home/karl/.config/nvim/init.lua'

-- Import my custom function to set the logo
local logo = setlogo()
dashboard.section.header.val = vim.split(logo, '\n')
dashboard.section.buttons.val = {
  dashboard.button('f', ' ' .. ' Find file', ':Telescope find_files <CR>'),
  dashboard.button('n', ' ' .. ' New file', ':ene <BAR> startinsert <CR>'),
  dashboard.button('r', ' ' .. ' Recent files', ':Telescope oldfiles <CR>'),
  dashboard.button('g', ' ' .. ' Find text', ':Telescope live_grep <CR>'),
  dashboard.button('c', ' ' .. ' Config', ':e' .. myConfig .. '<CR>'),
  dashboard.button('s', ' ' .. ' Restore Session', [[:LoadSession <cr>]]),
  dashboard.button('l', '󰒲 ' .. ' Lazy', ':Lazy<CR>'),
  dashboard.button('q', ' ' .. ' Quit', ':qa<CR>'),
}
for _, button in ipairs(dashboard.section.buttons.val) do
  button.opts.hl = 'AlphaButtons'
  button.opts.hl_shortcut = 'AlphaShortcut'
end
dashboard.opts.layout[1].val = 8

require('alpha').setup(dashboard.opts)

vim.api.nvim_create_autocmd('User', {
  pattern = 'LazyVimStarted',
  callback = function()
    local stats = require('lazy').stats()
    local ms = (math.floor(stats.startuptime * 100 + 0.5) / 100)
    dashboard.section.footer.val = '⚡ Neovim loaded ' .. stats.count .. ' plugins in ' .. ms .. 'ms'
    pcall(vim.cmd.AlphaRedraw)
  end,
})
