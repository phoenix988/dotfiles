-- Initlize plugins here
require("plugins.dashboard")
require("plugins.cmp.setup")
require("plugins.lspconfig.setup")
require("plugins.dap")
require("plugins.mason")
require("plugins.ollama")

-- Themes
require("plugins.themes.iceberg")
