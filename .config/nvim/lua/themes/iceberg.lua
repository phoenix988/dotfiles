-- this line for types, by hovering and autocompletion (lsp required)
-- will help you understanding properties, fields, and what highlightings the color used for
---@type Base46Table
local M = {}

M.base_30 = {
  white = "#D9E0EE",
  darker_black = "#191828",
  black = "#1E1D2D", --  nvim bg
  black2 = "#1E2132",
  one_bg = "#1E2132", -- real bg of onedark
  one_bg2 = "#1E2132",
  one_bg3 = "#1E2132",
  grey = "#5a5b5e",
  grey_fg = "#5a5b5e",
  grey_fg2 = "#555464",
  light_grey = "#818596",
  red = "#E98989",
  baby_pink = "#E27878",
  pink = "#F5C2E7",
  line = "#383747", -- for lines like vertsplit
  green = "#ABE9B3",
  vibrant_green = "#b6f4be",
  nord_blue = "#8bc2f0",
  blue = "#89B4FA",
  yellow = "#FAE3B0",
  sun = "#ffe9b6",
  purple = "#A093C7",
  dark_purple = "#7A6B9D",
  teal = "#89DCEB",
  orange = "#F4C6A2",
  cyan = "#8FC1C3",
  statusline_bg = "#1E1D2D",
  lightbg = "#2f2e3e",
  pmenu_bg = "#C0CA8E",
  folder_bg = "#84A0C6",
  lavender = "#c7d1ff",
}

M.base_16 = {
  base00 = "#1E1D2D",
  base01 = "#282737",
  base02 = "#2f2e3e",
  base03 = "#383747",
  base04 = "#414050",
  base05 = "#bfc6d4",
  base06 = "#ccd3e1",
  base07 = "#D9E0EE",
  base08 = "#F38BA8",
  base09 = "#F8BD96",
  base0A = "#FAE3B0",
  base0B = "#ABE9B3",
  base0C = "#89DCEB",
  base0D = "#89B4FA",
  base0E = "#CBA6F7",
  base0F = "#F38BA8",
}
-- OPTIONAL
-- overriding or adding highlights for this specific theme only 
-- defaults/treesitter is the filename i.e integration there, 

M.polish_hl = {
  defaults = {
    Comment = {
      bg = "#ffffff", -- or M.base_30.cyan
      italic = true,
    },
  },

  treesitter = {
    ["@variable"] = { fg = M.base_30.lavender },
    ["@property"] = { fg = M.base_30.teal },
    ["@variable.builtin"] = { fg = M.base_30.red },
  },
}

-- set the theme type whether is dark or light
M.type = "dark" -- "or light"

-- this will be later used for users to override your theme table from chadrc
M = require("base46").override_theme(M, "iceberg")

return M
