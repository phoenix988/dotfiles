local wk = require 'which-key'

-- Sets some Descriptions for keychords
wk.add({
  { "<leader>f", group = "Find Files" }, -- group
  { "<leader>D", group = "Diagnostic" }, -- group
  { "<leader>fg",  desc = "Grep For String", mode = "n" },
  { "<leader>fG",  desc = "Telescope Grep", mode = "n" },
  { "<leader>fF",  desc = "Format File", mode = "n" },
  --{ "<leader>f1", hidden = true }, -- hide this keymap
  { "<leader>n", group = "File Tree" }, -- group
  { "<leader>o", group = "Org Mode" }, -- group
  { "<leader>b", group = "Buffers" }, -- group
  { "<leader>h", group = "Help" }, -- group
  { "<leader>d", desc = "delete without yanking", mode = "n" }, -- group
  { "<leader>h1", hidden = true },
  { "<leader>h2", hidden = true },
  { "<leader>h3", hidden = true },
  { "<leader>h4", hidden = true },
  { "<leader>h5", hidden = true },
  { "<leader>h6", hidden = true },
  { "<leader>h7", hidden = true },
  { "<leader>h8", hidden = true },
  { "<leader>h9", hidden = true },
  { "<leader>g", group = "Git" }, -- group
  { "<leader>c", group = "Code" }, -- group
  { "<leader>t", group = "Tab" }, -- group
  { "<leader>tg", "<cmd>tabNext<cr>", desc = "Next Tab"},
  { "<leader>tG", "<cmd>tabprevious<cr>", desc = "Previous Tab"},
  { "<leader>p", group = "Project" }, -- group
  { "<leader>s", group = "Sessions" }, -- group
  { "<leader>sm", desc = "Sessions Menu" }, -- group
  { "<leader>ss", desc = "Save Session" }, -- group
  { "<leader>sl", desc = "Load Session" }, -- group
  { "<leader>m", group = "MarkDown" }, -- group
  { "<leader>w", group = "Word" }, -- group
  { "<leader>w", proxy = "<c-w>", group = "windows" }, -- proxy to window mappings
  { "<leader>b", group = "buffers", expand = function()
      return require("which-key.extras").expand.buf()
    end
  },
  {
    -- Nested mappings are allowed and can be added in any order
    -- Most attributes can be inherited or overridden on any level
    -- There's no limit to the depth of nesting
    mode = { "n", "v" }, -- NORMAL and VISUAL mode
    --{ "<leader>q", "<cmd>q<cr>", desc = "Quit" }, -- no need to specify mode since it's inherited
    --{ "<leader>w", "<cmd>w<cr>", desc = "Write" },
  }
})

