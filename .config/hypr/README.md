# Hyprland

![Hyprland](./images/hyprland.png)

## TABLE OF CONTENT

- [About My Hyprland](#about-my-hyprland)
  - [Dependencies](#dependencies)
  - [Installation](#installation)
  - [Important Keybindings](#important-keybindings)

## ABOUT MY HYPRLAND

Hyprland is a windowmanager manager based on the wayland compositer
and in my opinion it's a fantastic window manager. This is my configuration file for it
where you will see all my keybindings I prefer to use. In order to use this config fully
you will need some additonal software, like wofi,swaybg,grim waybar etc.
Also the terminal in this config is kitty so make sure to change that to your
prefered terminal so you can open a terminal in this windowmanager.

![Hyprland Desktop](./images/hyprland-desktop.png)

### DEPENDENCIES

If you want to use my config there is some things
that you will need to install. Of course you don't need them and install
your prefered software but then you need to customize my config.

- Grim (For screenshots)
- Swaybg (Setting Wallpaper)
- Swaylock (To lock the screen)
- Waybar (For the bar)
- Wofi or Rofi(Run launcher)

### INSTALLATION

Install all the packages you need to get started with this config

```bash
# I'm using paru but feel free to use any aur helper if you are on arch
paru -S --needed --noconfirm waybar-hyprland-git hyprland swaylock swayshot swaybg grim rofi lxappearance qt5ct qt6ct kvantum awesome-terminal-fonts adobe-source-code-pro-fonts ttf-jetbrains-mono \
 ttf-jetbrains-mono ttf-jetbrains-mono-nerd ttf-iosevka-nerd pyprland xdg-desktop-portal-wlr nwg-look 

# choose terminal you prefer default in the config is wezterm
paru -S kitty
paru -S wezterm


# Clones my repository
git clone https://github.com/phoenix988/hyprland.git $HOME/.config/hypr
```

#### OPTIONAL

If you want to install my collection of fonts that can come in handy
you can clone my repo where I store a collection of fonts I use for my configs

```bash
# Here you clone them to your user folder
git clone https://github.com/phoenix988/fonts.git $HOME/.local/share/fonts

# But you can also choose to move them to /usr/share/fonts to make it systemwide
sudo cp -r $HOME/fonts/\* /usr/share/fonts
```

## IMPORTANT KEYBINDINGS

This is just a quick recap of one of the most important
keybindings to know in order to use this config.
To get around quickly. If you know these keybindings
then you will have no problem to get around in my Hyprland.

### GENERAL
| Keybinding            | Description          |
| --------------------- | -------------------- |
| SUPER + Q             | Close Window         |
| SUPER + SHIFT + Q     | Exit Hyprland        |
| SUPER + F11     | Reload Waybar        |
| SUPER + F12     | Random Wallpaper         |
| SUPER + F2     | Kitty Help Sheet         |
| SUPER + F1     | Hyprland Help Sheet          |

### WINDOWS
| Keybinding | DESCRIPTION      |
| ---------- | ---------------- |
| SUPER + H  | Move focus left  |
| SUPER + L  | Move focus right |
| SUPER + K  | Move focus up    |
| SUPER + J  | Move focus down  |

| Keybinding        | DESCRIPTION       |
| ----------------- | ----------------- |
| SUPER + SHIFT + H | Move window left  |
| SUPER + SHIFT + L | Move window right |
| SUPER + SHIFT + K | Move window up    |
| SUPER + SHIFT + J | Move window down  |

| Keybinding       | DESCRIPTION         |
| ---------------- | ------------------- |
| SUPER + CTRL + H | Resize window left  |
| SUPER + CTRL + L | Resize window right |
| SUPER + CTRL + K | Resize window up    |
| SUPER + CTRL + J | Resize window down  |

### APPLICATIONS
| Keybinding            | Description          |
| --------------------- | -------------------- |
| SUPER + Enter         | Open Terminal            |
| SUPER + SHIFT + Enter | Open Filemanager     |
| SUPER + R             | Run Launcher             |
| SUPER + G             | Open Gimp             |
| SUPER + SHIFT + G             | Open Kdenlive             |
| SUPER + SHIFT + E             | Open dired in emacs             |
| SUPER + SHIFT + D     | Open Emacs           |
| SUPER + SHIFT + W     | Open Default browser |
| SUPER + SHIFT + V     | Open Virt-Manager |
