#!/usr/bin/env bash 
# 
# Using bash in the shebang rather than /bin/sh, which should
# be avoided as non-POSIX shell users (fish) may experience errors.
lxsession &

# Start emacs daemon
emacs --daemon &

#Set wallpaper
~/.local/share/scripts/activated/fehbg &
# feh --randomize --bg-fill /home/karl/Pictures/Wallpapers/* &

nm-applet &
pa-applet &
xfce4-power-manager &
flameshot &
steam -silent -no-browser &
blueman-applet &

# xfce4-screensaver &
# conky -c /home/karl/.config/conky/main &
