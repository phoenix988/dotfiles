import re
from libqtile.config import Match

# Set Workspace rules here
group_names = [
    ("1", {
        'layout': 'bsp',
        'matches': [Match(wm_class=re.compile(r"^(Brave)$"))]
    }),
    ("2", {
        'layout': 'bsp',
        'matches': [Match(wm_class=re.compile(r"^(neo)$"))]
    }),
    ("3", {
        'layout': 'bsp',
        'matches': [Match(wm_class=re.compile(r"^(Teamviewer)$"))]
    }),
    ("4", {
        'layout': 'max',
        'matches': [Match(wm_class=re.compile(r"^(lutris|heroic)$"))]
    }),
    ("5", {
        'layout': 'bsp',
        'matches': [Match(wm_class=re.compile(
            r"^(re.sonny.Tangram,crx_cifhbcnohmdccbgoicgdjpfamggdegmo|disk.yandex.com__client_disk)$"))]
    }),
    ("6", {
        'layout': 'max',
        'matches': [Match(wm_class=re.compile(r"^(discord|Franz)$"))]
    }),
    ("7", {
        'layout': 'bsp',
        'matches': [Match(wm_class=re.compile(r"^(Spotify)$"))]
    }),
    ("8", {
        'layout': 'treetab',
        'matches': [Match(wm_class=re.compile(
            r"^(crx__hnpfjngllnobngcgfapefoaidbinmjnm|obs|youtube.com|netflix.com)$"))]
    }),
    ("9", {
        'layout': 'bsp',
        'matches': [Match(wm_class=re.compile(
            r"^(gimp-2.10|Gimp|Cinelerra|Olive|kdenlive|resolve)$"))]
    })
]
