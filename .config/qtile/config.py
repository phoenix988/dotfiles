# ____  _                      _
# |  _ \| |__   ___   ___ _ __ (_)_  __
# | |_) | '_ \ / _ \ / _ \ '_ \| \ \/ /
# |  __/| | | | (_) |  __/ | | | |>  <
# |_|   |_| |_|\___/ \___|_| |_|_/_/\_\
# -*- coding: utf-8 -*-

# this is the main config file where everything bootstraps
# togheter you are not meant to mess with this file
# unless you know what you are doing

# Import qtile libaries
import os
import subprocess
from libqtile.config import (
        Click,
        Drag,
        Group,
        Key)

# from libqtile.command import lazy
from libqtile import hook
from libqtile.lazy import lazy

# Workspaces
from UserConfig.workspaces.icon import group_names

# Layouts
from UserConfig.layouts import layouts
from UserConfig.rules import floating_layout

# Scratchpad
from UserConfig.scratchpads import scratchpad

# keybindings
from UserConfig.binds import keys

from UserConfig.variables import Selected

# Widgets
from UserConfig.widgets.init import Render

# Allows you to input a name when adding treetab section
# Define super key as variable
mod = Selected.mod

groups = [Group(name, **kwargs) for name, kwargs in group_names]

# append scracthpads to group
groups.append(scratchpad)

# Do not touch this it allows you to switch between workspaces
# with mod + 1-9
for i, (name, kwargs) in enumerate(group_names, 1):
    # Switch to another group
    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))
    # Send current window to another group
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name)))

if __name__ in ["config", "__main__"]:
    screens = Render.init_screens()
    widgets_list = Render.init_widgets_list()
    widgets_screen1 = Render.init_widgets_screen1()
    widgets_screen2 = Render.init_widgets_screen2()


def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)


def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)


def window_to_previous_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group)


def window_to_next_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group)


def switch_screens(qtile):
    i = qtile.screens.index(qtile.current_screen)
    group = qtile.screens[i - 1].group
    qtile.current_screen.set_group(group)


# Makes the windows dragable/resizable with the mouse
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
# dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False

auto_fullscreen = True
focus_on_window_activation = "smart"


# Configure startup scripts
@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])
    subprocess.call([home + '/.config/qtile/autostart_custom.sh'])


wmname = "LG3D"
