# Dracula theme for qtile
# Author: Karl Fredin
class Catppuccin():
    layout_colors = [["#F5C2E7", "#F5C2E7"], #  0 -- Border color
                     ["#1E1E2E", "#1E1E2E"], #  1
                     ["#1E1E2E", "#1E1E2E"], #  2
                     ["#000000", "#000000"], #  3
                     ["#94E2D5", "#94E2D5"]] #  4

    colors = [["#1E1E2E", "#1E1E2E"], # 0 -- BG color
          ["#495469", "#495469"], # 1 -- Inactive workspace
          ["#F5C2E7", "#F5C2E7"], # 2
          ["#A6E3A1", "#A6E3A1"], # 3
          ["#74C7EC", "#74C7EC"], # 4 -- Workspace highlight
          ["#A4FFFF", "#A4FFFF"], # 5 -- Workspace FG
          ["#89B4FA", "#89B4FA"], # 6
          ["#F5C2E7", "#F5C2E7"], # 7
          ["#94e2d5", "#94e2d5"], # 8  -- weather widget
          ["#CBA6F7", "#CBA6F7"], # 9  -- df widget
          ["#A6E3A1", "#A6E3A1"], # 10 -- cpu widget
          ["#F38BA8", "#F38BA8"], # 11 -- temp widget
          ["#F5C2E7", "#F5C2E7"], # 12 -- memory widget
          ["#F9E2AF", "#F9E2AF"], # 13 -- update widget
          ["#94E2D5", "#94E2D5"], # 14 -- volume widget
          ["#f8f8f2", "#f8f8f2"]] # 15 -- linux icon


# Dracula theme for qtile
# Author: Karl Fredin
class Dracula():
    layout_colors = [["#bd92f8", "#bd92f8"], #  0 -- Border color
                     ["#2e3440", "#2e3440"], #  1
                     ["#1e1f28", "#1e1f28"], #  2
                     ["#000000", "#000000"], #  3
                     ["#ecbbfb", "#ecbbfb"]] #  4

    colors = [["#1e1f28", "#1e1f28"], # 0 -- BG color
              ["#6272A4", "#6272A4"], # 1 -- Inactive workspace
              ["#ff78c5", "#ff78c5"], # 2
              ["#50fa7b", "#50fa7b"], # 3
              ["#A372D8", "#A372D8"], # 4 -- Workspace highlight
              ["#A4FFFF", "#A4FFFF"], # 5 -- Workspace FG
              ["#ff78c5", "#ff78c5"], # 6
              ["#ecbbfb", "#ecbbfb"], # 7
              ["#FF92DF", "#FF92DF"], # 8  -- weather widget
              ["#D6ACFF", "#D6ACFF"], # 9  -- df widget
              ["#69FF94", "#69FF94"], # 10 -- cpu widget
              ["#FF6E6E", "#FF6E6E"], # 11 -- temp widget
              ["#FF79C6", "#FF79C6"], # 12 -- memory widget
              ["#FFFFA5", "#FFFFA5"], # 13 -- update widget
              ["#A4FFFF", "#A4FFFF"], # 14 -- volume widget
              ["#f8f8f2", "#f8f8f2"]] # 15 -- linux icon


# Iceberg theme for qtile
# Author: Karl Fredin

# colors for borders and layouts
class Iceberg():
    layout_colors = [["#A093C7", "#A093C7"],
                     ["#161821", "#161821"],
                     ["#161821", "#161821"],
                     ["#000000", "#000000"],
                     ["#6b7089", "#6b7089"]]


    # Color for the qtile bar
    colors = [["#161821", "#161821"],  # 0 -- BG color
              ["#495469", "#495469"],  # 1
              ["#91ACD1", "#91ACD1"],  # 2
              ["#C0CA8E", "#C0CA8E"],  # 3
              ["#425E86", "#425E86"],  # 4
              ["#C9A6CD", "#C9A6CD"],  # 5 -- Workspace occupied
              ["#81A1C1", "#81A1C1"],  # 6
              ["#8FC1C3", "#8FC1C3"],  # 7
              ["#E9B189", "#E9B189"],  # 8  -- weather widget
              ["#D4D7B6", "#D4D7B6"],  # 9  -- df widget
              ["#ADA0D3", "#ADA0D3"],  # 10 -- cpu widget
              ["#E98989", "#E98989"],  # 11 -- temp widget
              ["#8FBCBB", "#8FBCBB"],  # 12 -- memory widget
              ["#b48ead", "#b48ead"],  # 13 -- update widget
              ["#91acd1", "#91acd1"],  # 14 -- volume widget
              ["#d8dee9", "#d8dee9"]]  # 15 -- linux icon


# Nord theme for qtile
# Author: Karl Fredin
class Nord():
    layout_colors = [["#88C0D0", "#88C0D0"], #  0 -- Border color
                    ["#2e3440", "#2e3440"], #  1
                    ["#2E3440", "#2E3440"], #  2
                    ["#000000", "#000000"], #  3
                    ["#6B7089", "#6B7089"]] #  4



    colors = [["#2E3440", "#2E3440"], # 0 -- BG color
             ["#4C566A", "#4C566A"], # 1 -- Inactive workspace
             ["#88C0D0", "#88C0D0"], # 2
             ["#A3BE8C", "#A3BE8C"], # 3
             ["#81A1C1", "#81A1C1"], # 4 -- Workspace highlight
             ["#FFFACD", "#FFFACD"], # 5 -- Workspace FG
             ["#88C0D0", "#88C0D0"], # 6
             ["#ecbbfb", "#ecbbfb"], # 7
             ["#81A1C1", "#81A1C1"], # 8  -- weather widget
             ["#A3BE8C", "#A3BE8C"], # 9  -- df widget
             ["#EBCB8B", "#EBCB8B"], # 10 -- cpu widget
             ["#BF616A", "#BF616A"], # 11 -- temp widget
             ["#B48EAD", "#B48EAD"], # 12 -- memory widget
             ["#EBCB8B", "#EBCB8B"], # 13 -- update widget
             ["#81A1C1", "#81A1C1"], # 14 -- volume widget
             ["#FFFACD", "#FFFACD"]] # 15 -- linux icon


# Rose pine theme for qtile
# Author: Karl Fredin
# colors for borders and layouts
class Rose_Pine():
    layout_colors = [["#EBBCBA", "#EBBCBA"], # 0 -- Border color
                    ["#191724", "#191724"], # 1
                    ["#191724", "#191724"], # 2
                    ["#000000", "#000000"], # 3
                    ["#6b7089", "#6b7089"]] # 4


# Color for the qtile bar
    colors = [["#191724", "#191724"], # 0 -- BG color
            ["#6E6A86", "#6E6A86"], # 1
            ["#EBBCBA", "#EBBCBA"], # 2
            ["#31748F", "#31748F"], # 3
            ["#AB355A", "#AB355A"], # 4
            ["#EBBCBA", "#EBBCBA"], # 5
            ["#EBBCBA", "#EBBCBA"], # 6
            ["#EBBCBA", "#EBBCBA"], # 7
            ["#F6C177", "#F6C177"], # 8  -- weather widget
            ["#E0DEF4", "#E0DEF4"], # 9  -- df widget
            ["#31748F", "#31748F"], # 10 -- cpu widget
            ["#EB6F92", "#EB6F92"], # 11 -- temp widget
            ["#C4A7E7", "#C4A7E7"], # 12 -- memory widget
            ["#EBBCBA", "#EBBCBA"], # 13 -- update widget
            ["#9CCFD8", "#9CCFD8"], # 14 -- volume widget
            ["#F6C177", "#F6C177"]] # 15 -- linux icon


# TorkyoNight theme for qtile
# Author: Karl Fredin
# colors for borders and layouts
class Tokyo_Night():
    layout_colors = [["#7DCFFF", "#7DCFFF"], # 0 -- Border color
                     ["#1A1B26", "#1A1B26"], # 1
                     ["#1A1B26", "#1A1B26"], # 2
                     ["#000000", "#000000"], # 3
                     ["#6B7089", "#6B7089"]] # 4


# Color for the qtile bar
    colors = [["#1A1B26", "#1A1B26"], # 0 -- BG color
              ["#6B7089", "#6B7089"], # 1 -- Task tray bg
              ["#BB9AF7", "#BB9AF7"], # 2
              ["#9ECE6A", "#9ECE6A"], # 3
              ["#7AA2F7", "#7AA2F7"], # 4
              ["#4C3466", "#8264B3"], # 5 -- Workspace occupied
              ["#BB9AF7", "#BB9AF7"], # 6
              ["#BB9AF7", "#BB9AF7"], # 7
              ["#F6C177", "#F6C177"], # 8  -- weather widget
              ["#E0DEF4", "#E0DEF4"], # 9  -- df widget
              ["#9ECE6A", "#9ECE6A"], # 10 -- cpu widget
              ["#F7768E", "#F7768E"], # 11 -- temp widget
              ["#7DCFFF", "#7DCFFF"], # 12 -- memory widget
              ["#BB9AF7", "#BB9AF7"], # 13 -- update widget
              ["#E0AF68", "#E0AF68"], # 14 -- volume widget
              ["#1A1B26", "#1A1B26"]] # 15 -- linux icon
