;; Highlights
(package! beacon)
(package! rainbow-mode)
(package! ssh)

;; Navigation
(package! evil-tabs)
(package! harpoon)

;; Treemacs
(package! lsp-treemacs)
(package! treemacs-icons-dired)

;; Grep
(package! wgrep)

;; LSP
(package! go)
(package! company-box)
(package! company)
(package! lsp-mode)
(package! lua-mode)
(package! go-mode)
(package! lsp-ui)
(package! company-lsp)

;; python
(package! lsp-pyright)

;; Theming
(package! catppuccin-theme)
(package! autothemer)
(package! dired-open)
(package! peep-dired)

;; ICONS
(package! all-the-icons)
;;(package! all-the-icons-dired)
(package! all-the-icons-ivy-rich)

;; AI
(package! ellama)
(package! chatgpt-shell)
(package! org-ai)
(package! gptel)

;; startscreen
(package! dashboard)

;; IVY
(package! counsel)

;; ORG
(package! org-auto-tangle)
(package! org-bullets)

(package! mu4e)

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '( "jcs-elpa" . "https://jcs-emacs.github.io/jcs-elpa/packages/"))
