##____  _                      _
#|  _ \| |__   ___   ___ _ __ (_)_  __
#| |_) | '_ \ / _ \ / _ \ '_ \| \ \/ /
#|  __/| | | | (_) |  __/ | | | |>  <
#|_|   |_| |_|\___/ \___|_| |_|_/_/\_\
#
# -*- coding: utf-8 -*-
# Path to your oh-my-zsh installation.
export ZSH="$HOME/.config/zsh/config"
export ZSH_CUSTOM_PLUGIN="$ZSH/plugins"

# PM == package manager
export PM="paru"

# Install oh-my-zsh if it is not installed
# if [ ! -d "$ZSH" ]; then
#     printf "oh my zsh is not installed... installing \n"
#     git clone https://gitlab.com/phoenix988/setup.git $HOME/setup
#     bash $HOME/setup/scripts/ohmyzsh.sh
# 
#     rm -rf $HOME/setup
# fi

export HISTCONTROL=ignoreboth:erasedups
export HISTFILE=$HOME/.config/zsh/zsh_history

# Source oh my zsh
source ~/.config/zsh/config/init.sh

# source secret environment variables that I do not want to share
# if that file does exist
[ -f "$HOME"/.env ] &&  source $HOME/.env

# Function for installation of plugins
install_plugins() {
    if [ ! -d  "$ZSH_CUSTOM_PLUGIN/$1" ] ; then
        printf "installing plugin..."
        git clone "https://github.com/$2" "$ZSH_CUSTOM_PLUGIN/$1"
        clear 
    fi
}

# Install the plugisn if the dont exist
install_plugins "zsh-users" "zsh-users/zsh-completions"
install_plugins "zsh-vim-mode" "softmoth/zsh-vim-mode"
install_plugins "zsh-autosuggestions" "zsh-users/zsh-autosuggestions"
install_plugins "zsh-syntax-highlighting" "zsh-users/zsh-syntax-highlighting"

# Source plugins
source $ZSH_CUSTOM_PLUGIN/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source $ZSH_CUSTOM_PLUGIN/zsh-autosuggestions/zsh-autosuggestions.zsh
source $ZSH_CUSTOM_PLUGIN/zsh-vim-mode/zsh-vim-mode.plugin.zsh
source $ZSH_CUSTOM_PLUGIN/zsh-users/zsh-completions.plugin.zsh

autoload -U compinit && compinit

command_exist() {
        type "$1" &> /dev/null;
}

# Set your FZF theme
FZF_THEME="iceberg"

# Set your prompt ttheme here I am using starship
command_exist starship && eval "$(starship init zsh)"

# Set zsh theme if you dont use any other prompt
#ZSH_THEME='nord'

# Use zoxide if it is installed
command_exist zoxide && eval "$(zoxide init --cmd cd zsh)"

# pfetch and neofetch
if [ -f /usr/bin/fastfetch ] ; then
    fastfetch
elif [ -f /usr/bin/neofetch ]  ; then
    neofetch
fi

# Add function to add to my path 
add_to_path() {
    add_path="$1"
    path_exist=$(echo $PATH | sed 's/:/\n/g' | grep $add_path)
    [ -z "$path_exist" ] &&  PATH="$PATH:$add_path"
}

add_to_path "$XDG_DATA_HOME/go/bin"
add_to_path "$HOME/.config/emacs/bin/"
add_to_path "$HOME/.local/share/dmenu"
add_to_path "$HOME/.scripts/activated"
add_to_path "$HOME/.local/share/npm/bin"
add_to_path "$HOME/.local/bin"


[ "$TERM" = "xterm-color" ] && export TERM=xterm-256color

bindkey '^ ' autosuggest-accept

ZSH_HIGHLIGHT_STYLES[default]='fg=3'
ZSH_HIGHLIGHT_STYLES[precommand]='fg=5,underline'
ZSH_HIGHLIGHT_STYLES[command]='fg=4'
ZSH_HIGHLIGHT_STYLES[builtin]='fg=6'
ZSH_HIGHLIGHT_STYLES[alias]='fg=4'
ZSH_HIGHLIGHT_STYLES[path]='fg=5'

# Function to easily extract any file using ex
ex ()
{
 if [ -f "$1" ]; then 
     case $1 in
         *.tar.bz2)   tar xjf  $1   ;;
         *.tar.gz)    tar xzf  $1   ;;
         *.bz2)       bunzip2  $1   ;;
         *.rar)       unrar x  $1   ;;
          *.gz)       gunzip   $1   ;;
         *.tar)       tar xf   $1   ;;
         *.tbz2)      tar xjf  $1   ;;
         *.tgz)       tar xzf  $1   ;;
         *.zip)       unzip    $1   ;;
         *.Z)         uncompress $1 ;;
         *.7z)        7z x     $1   ;;
         *.deb)       ar x     $1   ;;
         *.tar.zst)    unzstd  $1   ;;
         *.tar.xz)     tar xf  $1   ;;
         *)            echo "$1 Cannot be extracted vi ex()"
      esac
    else
       echo "$1 is not a valid file"
       fi  
}

# Import fzf theme
[ -d $HOME/.config/fzf/$FZF_THEME ] && source ~/.config/fzf/$FZF_THEME.sh

# Vim keybindings for zsh
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

# Sets date variable
export date=$(date +%d-%h-%Y-%H-%M)
alias dateFormat=date +%d-%h-%Y-%H-%M

alias sou="source ~/.config/zsh/.zshrc"

# Configure all my aliases
[ -f $HOME/.config/alias/alias.sh ] && source $HOME/.config/alias/alias.sh


### Custom functions / keybindings ###
# Function to fuzzy search history
fzf-history-widget() {
  local selected_command
  selected_command=$(history | fzf --tac --no-sort +m --tiebreak=index | sed 's/ *[0-9]* *//')
  if [[ -n $selected_command ]]; then
    LBUFFER=$selected_command
  fi
  zle reset-prompt
}

# Function to edit config files
edit-config-files() {
  local selected_command
  selected_command=$(cat ~/.cache/dm-edit | fzf --tiebreak=index)
  if [[ -n $selected_command ]]; then
    nvim $selected_command
  fi
  zle reset-prompt
}

tmux-sessions() {
  tmux choose-session
  zle reset-prompt
}

# search pacman repos for packages to install
search-install-package() {
    $PM -Slq | fzf --multi --preview "$PM -Si {1}" | xargs -ro $PM -S
}

remove-installed-package() {
    $PM -Qq | fzf --multi --preview "$PM -Qi {1}" | xargs -ro $PM -Rns
}

clear-screen() {
  zle clear-screen              # Clear the screen
  BUFFER="clear"    # Set the command to be typed out
  zle accept-line               # Simulate pressing Enter
}

move-widget() {
    cdi
    zle accept-line               # Simulate pressing Enter
}

# Bind Ctrl+R to the fzf fuzzy history search
zle -N fzf-history-widget
bindkey '^R' fzf-history-widget

# Bind Ctrl+E to quickly open config files 
zle -N edit-config-files
bindkey '^E' edit-config-files

zle -N tmux-sessions
bindkey '^T' tmux-sessions

zle -N search-install-package
bindkey '^P' search-install-package

zle -N remove-installed-package
bindkey '^W' remove-installed-package

zle -N clear-screen
bindkey '^L' clear-screen

# Bind Ctrl+f to insert 'zi' followed by a newline
zle -N move-widget
bindkey '^F' move-widget
######################################
### Custom functions / keybindings ###
############# END ####################



    

# bun completions
[ -s "/home/karl/.bun/_bun" ] && source "/home/karl/.bun/_bun"
